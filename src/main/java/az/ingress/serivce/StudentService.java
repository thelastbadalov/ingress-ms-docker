package az.ingress.serivce;

import az.ingress.dto.StudentRequestDto;
import az.ingress.dto.StudentResponseDto;
import az.ingress.exception.EntityNotFoundException;
import az.ingress.mapper.StudentMapper;
import az.ingress.model.Student;
import az.ingress.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    public void create(StudentRequestDto studentRequestDto) {
        Student student = studentMapper.toEntity(studentRequestDto);
        studentRepository.save(student);
    }

    public List<StudentResponseDto> getAll() {
        List<Student> students = studentRepository.findAll();
        return students.stream()
                .map(studentMapper::toResponse).toList();
    }

    public StudentResponseDto getById(Long id) {
        Student student = findStudent(id);
        return studentMapper.toResponse(student);
    }

    public void update(Long id, StudentRequestDto studentRequestDto) {
        Student student = findStudent(id);
        studentMapper.update(student, studentRequestDto);
        studentRepository.save(student);
    }

    public void delete(Long id) {
        Student student = findStudent(id);
        studentRepository.delete(student);
    }

    private Student findStudent(Long id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Student.class, id));
    }

}
