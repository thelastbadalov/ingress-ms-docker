package az.ingress.controller;

import az.ingress.common.GenericResponse;
import az.ingress.dto.StudentRequestDto;
import az.ingress.dto.StudentResponseDto;
import az.ingress.serivce.StudentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/students")
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public GenericResponse<Void> create(@RequestBody @Valid StudentRequestDto studentRequestDto) {
        studentService.create(studentRequestDto);
        return GenericResponse.success();
    }

    @GetMapping("/{id}")
    public GenericResponse<StudentResponseDto> getById(@PathVariable Long id) {
        StudentResponseDto student = studentService.getById(id);
        return GenericResponse.success(student);
    }

    @GetMapping
    public GenericResponse<List<StudentResponseDto>> getAll() {
        List<StudentResponseDto> students = studentService.getAll();
        return GenericResponse.success(students);
    }

    @PutMapping("/{id}")
    public GenericResponse<Void> update(@PathVariable Long id,
                                        @RequestBody @Valid StudentRequestDto studentRequestDto) {
        studentService.update(id, studentRequestDto);
        return GenericResponse.success();
    }

    @DeleteMapping("/{id}")
    public GenericResponse<Void> delete(@PathVariable Long id) {
        studentService.delete(id);
        return GenericResponse.success();
    }

}
