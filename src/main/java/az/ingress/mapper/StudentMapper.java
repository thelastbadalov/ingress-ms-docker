package az.ingress.mapper;

import az.ingress.dto.StudentRequestDto;
import az.ingress.dto.StudentResponseDto;
import az.ingress.model.Student;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {

    Student toEntity(StudentRequestDto studentRequestDto);

    StudentResponseDto toResponse(Student student);

    void update(@MappingTarget Student entity, StudentRequestDto updateStudentRequestDto);

}
