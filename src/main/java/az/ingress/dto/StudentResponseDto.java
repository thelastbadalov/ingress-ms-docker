package az.ingress.dto;

import lombok.Data;

@Data
public class StudentResponseDto {

    private Long id;
    private String name;
    private String surname;
    private Integer age;

}
