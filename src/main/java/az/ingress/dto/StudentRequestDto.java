package az.ingress.dto;

import jakarta.validation.constraints.Min;
import lombok.Data;

@Data
public class StudentRequestDto {

    private String name;
    private String surname;

    @Min(value = 1, message = "Age must be greater than or equal to 1")
    private Integer age;

}
