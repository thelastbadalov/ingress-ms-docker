package az.ingress.exception;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(Class entity, String message) {
        super(entity.getSimpleName() + " " + message);
    }

    public EntityNotFoundException(Class clazz, Object id) {
        super(clazz.getSimpleName() + " not found with given id: " + id);
    }

}
