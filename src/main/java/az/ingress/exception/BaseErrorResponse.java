package az.ingress.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


@Data
@AllArgsConstructor
public class BaseErrorResponse {

    private String message;
    private String code;
    private String reason;
    private List<ValidationFailure> validationFailures;

    public BaseErrorResponse(String message, String code, String reason) {
        this.message = message;
        this.code = code;
        this.reason = reason;
    }

}
