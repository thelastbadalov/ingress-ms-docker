package az.ingress.exception;

public enum IngressErrorCode {

    INGRESS_001,
    INGRESS_002,
    INGRESS_003
}
