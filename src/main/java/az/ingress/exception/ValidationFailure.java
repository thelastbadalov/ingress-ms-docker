package az.ingress.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ValidationFailure {

    private String source;
    private String message;

}