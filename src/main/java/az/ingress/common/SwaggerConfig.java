package az.ingress.common;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {


    @Bean
    public OpenAPI openApiInformationLocal() {
        return getOpenAPI();
    }

    private OpenAPI getOpenAPI() {
        return new OpenAPI().components(null).info(getInfo())
                .addSecurityItem(new SecurityRequirement().addList("bearerToken"));
    }

    private Info getInfo() {
        Contact contact = new Contact()
                .name("INGRESS");
        return new Info()
                .contact(contact)
                .description("API documentation of ingress-ms.")
                .title("ingress-ms")
                .version("V1.0.0")
                .license(new License().name("Apache 2.0").url("https://springdoc.org"));
    }

}
